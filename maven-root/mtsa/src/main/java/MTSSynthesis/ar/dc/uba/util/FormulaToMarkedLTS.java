package MTSSynthesis.ar.dc.uba.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import MTSTools.ac.ic.doc.commons.collections.InitMap;
import MTSTools.ac.ic.doc.commons.collections.InitMap.Factory;
import MTSTools.ac.ic.doc.commons.relations.Pair;
import MTSTools.ac.ic.doc.mtstools.model.impl.MarkedLTSImpl;
import MTSSynthesis.ar.dc.uba.model.condition.AndFormula;
import MTSSynthesis.ar.dc.uba.model.condition.Fluent;
import MTSSynthesis.ar.dc.uba.model.condition.FluentPropositionalVariable;
import MTSSynthesis.ar.dc.uba.model.condition.Formula;
import MTSSynthesis.ar.dc.uba.model.condition.NotFormula;
import MTSSynthesis.ar.dc.uba.model.condition.OrFormula;
import MTSSynthesis.ar.dc.uba.model.language.Symbol;


/** This class allows translating boolean formulas over fluents into an LTS
 *  with marked states. */
public class FormulaToMarkedLTS {
	
	/** Enumeration of supported connectives. */
	private enum BinaryOperation { AND, OR }
	
	
	/** Returns the LTS representing the formula A => G, where A is a
	 *  conjunction of assumptions and G is the conjunction of guarantees. */
	public MarkedLTSImpl<Long, String> translate(List<Formula> assumptions, List<Formula> guarantees) {
		Formula formula = conjunct(guarantees);
		if (!assumptions.isEmpty()) {
			Formula assumption = conjunct(assumptions);
			formula = new OrFormula(new NotFormula(assumption), formula);
		}
		return translate(formula);
	}
	
	
	/** Returns the formula resulting from the conjunction of smaller formulas. */
	private Formula conjunct(List<Formula> formulas) {
		Formula result = formulas.get(0);
		for (int i = 1; i < formulas.size(); i++)
			result = new AndFormula(result, formulas.get(i));
		return result;
	}
	
	
	/** Returns the LTL representing a given formula. */
	public MarkedLTSImpl<Long, String> translate(Formula formula) {
		MarkedLTSImpl<Long, String> result = null;

		if (formula instanceof FluentPropositionalVariable) {
			result = fluent(((FluentPropositionalVariable) formula).getFluent());
		} else if (formula instanceof NotFormula) {
			result = not(((NotFormula) formula).getFormula());
		} else if (formula instanceof AndFormula) {
			AndFormula andFormula = (AndFormula) formula;
			result = binary(BinaryOperation.AND, andFormula.getLeftFormula(), andFormula.getRightFormula());
		} else if (formula instanceof OrFormula) {
			OrFormula orFormula = (OrFormula) formula;
			result = binary(BinaryOperation.OR, orFormula.getLeftFormula(), orFormula.getRightFormula());
		} else if (formula == Formula.FALSE_FORMULA) {
			result = new MarkedLTSImpl<Long, String>(0L);
		} else if (formula == Formula.TRUE_FORMULA) {
			result = new MarkedLTSImpl<Long, String>(0L);
			result.mark(0L);
		} else {
			throw new RuntimeException("Invalid formula " + formula);
		}
		result.removeAction("tau");

		return result;
	}

	
	/** Returns the LTS representing a single fluent. */
	private MarkedLTSImpl<Long, String> fluent(Fluent fluent) {
		boolean initial = fluent.isInitialValue();
		MarkedLTSImpl<Long, String> result = new MarkedLTSImpl<Long, String>(initial ? 2L : 0L);
		result.addState(0L);
		result.addState(1L);
		result.getMarkedStates().add(1L);
		for (Symbol s : fluent.getInitiatingActions()) {
			result.addAction(s.toString());
			result.addTransition(0L, s.toString(), 1L);
			result.addTransition(1L, s.toString(), 1L);
		}
		for (Symbol s : fluent.getTerminatingActions()) {
			result.addAction(s.toString());
			result.addTransition(1L, s.toString(), 0L);
			result.addTransition(0L, s.toString(), 0L);
		}
		if (initial) {
			result.addState(2L);
			for (Symbol s : fluent.getInitiatingActions())
				result.addTransition(2L, s.toString(), 1L);
			for (Symbol s : fluent.getTerminatingActions())
				result.addTransition(2L, s.toString(), 0L);
		}
		return result;
	}

	
	/** Returns the LTS representing the negation of a formula. */
	private MarkedLTSImpl<Long, String> not(Formula formula) {
		MarkedLTSImpl<Long, String> result = translate(formula);
		Set<Long> states = new HashSet<>(result.getStates());
		Set<Long> marked = result.getMarkedStates();
		states.removeAll(marked);
		marked.clear();
		marked.addAll(states);
		return result;
	}

	
	/** Returns the LTS representing the application of a binary operation between two formulas. */
	private MarkedLTSImpl<Long, String> binary(BinaryOperation op, Formula leftFormula, Formula rightFormula) {
		MarkedLTSImpl<Long, String> left = translate(leftFormula);
		MarkedLTSImpl<Long, String> right = translate(rightFormula);
		Pair<Long, Long> initial = Pair.create(left.getInitialState(), right.getInitialState());
		Map<Pair<Long, Long>, Long> states = new InitMap<>(new Factory<Long>() {
			long next = 0L;
			public Long newInstance() {
				return next++;
			}
		});
		MarkedLTSImpl<Long, String> result = new MarkedLTSImpl<Long, String>(states.get(initial));
		result.addActions(left.getActions());
		result.addActions(right.getActions());

		List<Pair<Long, Long>> pending = new ArrayList<>();
		pending.add(initial);
		while (!pending.isEmpty()) {
			Pair<Long, Long> current = pending.remove(pending.size() - 1);
			for (Pair<String, Long> transition : left.getTransitions(current.getFirst())) {
				String label = transition.getFirst();
				Long leftTarget = transition.getSecond();
				Set<Long> rightTargets = right.getTransitions(current.getSecond()).getImage(label);
				for (Long rightTarget : rightTargets) {
					Pair<Long, Long> child = Pair.create(leftTarget, rightTarget);
					Long source = states.get(current);
					Long target = states.get(child);
					if (result.addState(target)) {
						pending.add(child);
						switch (op) {
							case AND:
								if (left.isMarked(leftTarget) && right.isMarked(rightTarget))
									result.mark(target);
								break;
							case OR:
								if (left.isMarked(leftTarget) || right.isMarked(rightTarget))
									result.mark(target);
								break;
						}
					}
					result.addTransition(source, label, target);
				}
			}
		}

		return result;
	}

}
