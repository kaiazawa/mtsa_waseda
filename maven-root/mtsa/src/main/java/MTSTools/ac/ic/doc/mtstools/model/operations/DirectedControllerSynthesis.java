package MTSTools.ac.ic.doc.mtstools.model.operations;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import static java.util.Collections.*;

import MTSTools.ac.ic.doc.commons.collections.BidirectionalMap;
import MTSTools.ac.ic.doc.commons.collections.InitMap;
import MTSTools.ac.ic.doc.commons.collections.InitMap.Factory;
import MTSTools.ac.ic.doc.commons.collections.QueueSet;
import MTSTools.ac.ic.doc.commons.relations.BinaryRelation;
import MTSTools.ac.ic.doc.commons.relations.BinaryRelationImpl;
import MTSTools.ac.ic.doc.commons.relations.OrdPair;
import MTSTools.ac.ic.doc.commons.relations.Pair;
import MTSTools.ac.ic.doc.mtstools.model.LTS;
import MTSTools.ac.ic.doc.mtstools.model.impl.LTSImpl;
import MTSTools.ac.ic.doc.mtstools.model.impl.MarkedLTSImpl;



/** This class contains the logic to synthesize a controller for
 *  a deterministic environment using an informed search procedure. */
public class DirectedControllerSynthesis<State, Action> {
	
	/** Constant used to represent an infinite distance to the goal. */
	public static final int INF = Integer.MAX_VALUE;
	
	/** Constant used to represent an undefined distance to the goal. */
	public static final int UNDEF = -1;
	
	/** Indicates the abstraction to use in order to compute the heuristic. */
	public static AbstractionMode mode = AbstractionMode.Ready;
	
	/** List of LTS that compose the environment. */
	private List<LTS<State,Action>> ltss;
	
	/** The number of intervening LTSs. */
	private int ltssSize;
	
	/** Set of controllable actions. */
	private Set<Action> controllable;
	
	/** Indicates whether the procedure tries to reach a goal state (by default we look for live controllers). */
	private boolean reachability = false;
	
	/** Indicates whether the procedure consider a non-blocking requirement (by default we consider a stronger goal). */
	private boolean nonblocking = false;
	
	/** Environment alphabet (implements perfect hashing for actions). */
	private Alphabet alphabet;
	
	/** Set of transitions enabled by default by each LTS. */
	private TransitionSet base;
	
	/** Auxiliary transitions allowed by a given compostate. */
	private TransitionSet allowed;
	
	/** Last states used to update the allowed transitions. */
	private List<State> facilitators;
	
	/** Abstraction used to rank the transitions from a state. */
	private Abstraction abstraction;
	
	/** Cache of heuristic states ordered by creation order (used for perfect hashing). */
	private List<HState> stash;
	
	/** Cache of heuristic states used to reduce dynamic allocations. */
	private Map<Integer, HashMap<State, Integer>> cache;
	
	/** Queue of open states, the most promising state should be expanded first. */
	private Queue<Compostate> open;
	
	/** Queue of states evaluated with the heuristic yet to be expanded.*/
	private Queue<Compostate> evaluated;
	
	/** Cache of states mapped from their basic components. */
	private Map<List<State>,Compostate> compostates;
	
	/** List of reachable states after a transition (used during expansion). */
	private Deque<Set<State>> transitions;
	
	/** List of ancestors of an state (used during propagation). */
	private Deque<Pair<Status,BinaryRelation<HAction,Compostate>>> ancestorTransitions;
	
	/** Set of visited during iteration. */
	private Set<Compostate> visited;
	
	/** Set of gathered marked states. */
	private Set<Compostate> marked;
	
	/** Set of states in a loop. */
	private Set<Compostate> loop;
	
	/** Set of states in a CCC. */
	private Set<Compostate> ccc;
	
	/** Set of ancestor states with remaining options. */
	private Set<Compostate> unsettled;
	
	/** Directed acyclic graph from a successor state to a precursor state. */
	private BidirectionalMap<Compostate, Compostate> dag;
	
	/** List of ancestor states considered during the construction of the dag. */
	private List<Compostate> ancestorStates;
	
	/** List of descendants of an state (used to close unnecessary descendants). */
	private Deque<Compostate> descendants;
	
	/** Contains the marked states per LTS. */
	private List<Set<State>> defaultTargets;
	
	/** Initial state. */
	private Compostate initial;
	
	/** Statistic information about the procedure. */
	private Statistics statistics = new Statistics();
	
	
	/** This enum contains the possible status for a state. */
	private static enum Status {
		ERROR(-1, "ERROR"),
		NONE(0, "NONE"),
		STRONG(1, "STRONG"),
		WEAK(2, "WEAK");
		private final int precedence;
		private final String name;
		private Status(int p, String n) {
			precedence = p;
			name = n;
		}
		@Override
		public String toString() {
			return name;
		}
	}
	
	
	/** This method starts the directed synthesis of a controller.
	 *  @param names, the names of the LTSs (for debugging).
	 *  @param ltss, a list of MarkedLTSs that compose the environment. 
	 *  @param controllalbe, the set of controllable actions.
	 *  @param reachability, a boolean indicating whether to pursue reachability or liveness.
	 *  @param nonblocking, a boolean indicating whether to consider the nonblocking requirement.
	 *  @return the controller for the environment in the form of an LTS that
	 *      when composed with the environment reaches the goal, only by
	 *      enabling or disabling controllable actions and monitoring
	 *      uncontrollable actions. Or null if no such controller exists. */
	@SuppressWarnings("unchecked")
	public LTS<Long,Action> synthesize(
			List<String> names,
			List<LTS<State,Action>> ltss,
			Set<Action> controllable,
			boolean reachability,
			boolean nonblocking)
	{
		if (mode == AbstractionMode.Dummy)
			return (LTS<Long,Action>)ltss.get(0);
		
		this.ltss = ltss;
		this.ltssSize = ltss.size();
		this.controllable = controllable;
		this.reachability = reachability;
		this.nonblocking = nonblocking;
		
		statistics.clear();
		statistics.start();
		
		open = new PriorityQueue<>();
		evaluated = new LinkedList<>();
		
		compostates = new HashMap<>();
		transitions = new ArrayDeque<>(ltss.size());
		ancestorTransitions = new ArrayDeque<>();
		visited = new HashSet<>();
		marked = new HashSet<>();
		loop = new HashSet<>();
		ccc = new HashSet<>();
		unsettled = new HashSet<>();
		dag = new BidirectionalMap<>();
		ancestorStates = new ArrayList<>();
		descendants = new ArrayDeque<>();

		alphabet = new Alphabet();
		base = new TransitionSet(alphabet);
		allowed = base.clone();
		defaultTargets = buildDefaultTargets();
		
		stash = new ArrayList<>(ltss.size());
		cache = new HashMap<>();
		abstraction = mode == AbstractionMode.Monotonic ? new MonotonicAbstraction() : new ReadyAbstraction();
		
		initial = buildInitialState();
		initial.open();
		
		while (checkContinue()) {
			if (open.isEmpty()) {
				setError(initial);
			} else {
				doEval();
				doOpen();
			}
		}
		
		statistics.end();
		LTS<Long,Action> result = buildController();
		
		return result;
	}
	
	
	/** Indicates whether the exploration should continue or not. */
	private boolean checkContinue() {
		return initial.isStatus(Status.NONE) || (nonblocking && initial.isStatus(Status.WEAK) && initial.hasValidRecommendation());
	}
	
	
	/** Returns a list of marked states per LTS. */
	private List<Set<State>> buildDefaultTargets() {
		List<Set<State>> result = new ArrayList<>();
		for (int i = 0; i < ltssSize; ++i) {
			LTS<State,Action> lts = ltss.get(i);
			Set<State> markedStates = new HashSet<>();
			if (lts instanceof MarkedLTSImpl) {
				markedStates.addAll(((MarkedLTSImpl<State,Action>)lts).getMarkedStates());
			} else {
				markedStates.addAll(lts.getStates());
				markedStates.remove(-1L); // -1 is never marked since it is used to represent errors
			}
			result.add(markedStates);
		}
		return result;
	}
	
	
	/** Returns the statistic information about the procedure. */
	public Statistics getStatistics() {
		return statistics;
	}
	
	
	/** Creates (or retrieves from cache) a state in the composition given
	 *  a list with its base components.
	 *  @param states, a list of states with one state per LTS in the
	 *         environment, the position of each state in the list (its index)
	 *         reflects to which LTS that state belongs. */
	private Compostate buildCompostate(List<State> states) {
		Compostate result = compostates.get(states);
		if (result == null) {
			result = new Compostate(states);
			compostates.put(states, result);
			statistics.incExpanded();
		}
		return result;
	}
	
	
	/** Creates the controller's initial state. */
	private Compostate buildInitialState() {
		List<State> states = new ArrayList<>(ltss.size());
		for (LTS<State,Action> lts : ltss)
			states.add(lts.getInitialState());
		Compostate result = buildCompostate(states); // for non-deterministic LTS I need the tau-closure as a set of initial states
		if (result.marked/*isMarked()*/)
			result.addTargets(result);
		result.setDepth(0);
		return result;
	}
	
	
	/** Evaluates through the heuristic the next state to be opened (if necessary). */
	private void doEval() {
		Compostate compostate = open.remove();
		if (compostate.isLive())
			abstraction.eval(compostate);
		evaluated.add(compostate);
	}
	
	
	/** Expands the next recommendation of an open state and opens its child.
	 *  If no further recommendations are available the state is marked as an
	 *  error, and the error is propagated to its ancestors.
	 *  If the state is controllable and has further recommendations the parent
	 *  state is left open and competes with its children in the open queue.
	 *  This avoids a DFS type of search. On the other hand, uncontrollable
	 *  states are never left opened since all its children need to be explored
	 *  eventually, thus a DFS to a failure is used instead. */
	private void doOpen() {
		Compostate compostate = evaluated.remove();
		if (!compostate.isLive() || compostate.getStates() == null)
			return;
		compostate.inOpen = false;
		if (!compostate.hasValidRecommendation()) {
			if (isError(compostate) || compostate.getChildren().isEmpty()) {
				addAncestors(compostate, Status.ERROR);
				propagate();
			} else if (isGoal(compostate)) {
				addAncestors(compostate, Status.STRONG);
				propagate();
			}
		} else {
			Recommendation recommendation = compostate.nextRecommendation();
			expand(compostate, recommendation);
			if (compostate.isControlled() && compostate.hasValidRecommendation() && !isGoal(compostate))
				compostate.open();
		}
	}
	
	
	/** Expands a state following a given recommendation from a parent compostate.
	 *  Internally this populates the transitions and expanded lists. */
	private void expand(Compostate compostate, Recommendation recommendation) {
		HAction action = recommendation.getAction();
		List<State> parentStates = compostate.getStates();
		int size = parentStates.size();
		for (int i = 0; i < size; ++i) {
			Set<State> image = ltss.get(i).getTransitions(parentStates.get(i)).getImage(action.getAction());
			if (image == null || image.isEmpty()) {
				if (ltss.get(i).getActions().contains(action.getAction())) { // tau does not belong to the lts alphabet, yet it may have a valid image, but I do not want taus at this stage
					transitions.clear();
					System.err.println("Invalid action: " + action);
					return; // do not expand an state through an invalid action
				}
				image = singleton(parentStates.get(i));
			}
			transitions.add(image);
		}
		List<State> childStates = new ArrayList<>();
		for (Set<State> singleton : transitions) // Note: Cartesian product for non-deterministic systems
			childStates.add(singleton.iterator().next());
		transitions.clear();
		Compostate child = buildCompostate(childStates);
		compostate.addChild(action, child);
		child.addParent(action, compostate);
		
		child.setTargets(compostate.getTargets());
		if (child.marked/*isMarked()*/)
			child.addTargets(child);
		
		open(compostate, recommendation, child);
	}

	
	/** Opens a given child state following a recommendation from a parent.
	 *  This method distinguishes between reachability and liveness.
	 *  If the child to "open" is a goal (or closes a loop over a marked state)
	 *  it propagates the goal through the ancestors. If the child is an error
	 *  (or closes a loop over non-marked states) it propagates an error.
	 *  If the child is already live this method does nothing, otherwise the
	 *  child is evaluated with the heuristic and opened. */
	private void open(Compostate parent, Recommendation recommendation, Compostate child) {
		if (reachability) { // for reachability it is enough to get to a goal once
			if (isGoal(child)) { // if we reach a state we already know is a goal we propagate a goal
				propagate(Status.STRONG, child, recommendation.getAction(), parent);
			} else if (isError(child)) { // if we reach a state known to be an error propagate an error
				propagate(Status.ERROR, parent, recommendation.getAction(), child);
			} else if (child.marked/*isMarked()*/) { // if we reach a marked state we propagate a goal
				if (nonblocking) {
					buildAncestorsDAG(child, parent); // if nonblocking we might need to close weak loops
					gatherLoopStates(parent, child);
				}
				setGoal(child, 0);
				propagate(Status.STRONG, child, recommendation.getAction(), parent);
				if (nonblocking)
					clearLoopDetection();
			} else {
				if (child.isEvaluated())
					buildAncestorsDAG(child, parent);
				if (!dag.getK(child).isEmpty()) { // if closing a loop (not on a goal) propagate unsettled
					gatherLoopStates(parent, child);
					propagate(Status.WEAK, child, recommendation.getAction(), parent);
					clearLoopDetection();
				} else { // otherwise we keep exploring
					if (!child.isEvaluated())
						abstraction.eval(child);
					if (child.hasValidRecommendation()) {
						child.open();
					} else if (!child.hasStatusChild(Status.NONE)) {
						setError(child);
						propagate(Status.ERROR, child, recommendation.getAction(), parent);
					}
				}
			}
			
		} else { // for live controllers we need to close a loop
			if (isGoal(child)) { // if we reach a state we already know is a goal we propagate a goal
				propagate(Status.STRONG, child, recommendation.getAction(), parent);
			} else if (isError(child)) { // if we reach a state known to be an error propagate an error
				propagate(Status.ERROR, child, recommendation.getAction(), parent);
			} else {
				if (child.isEvaluated()) // if the child has already been considered then we might be closing a loop
					buildAncestorsDAG(child, parent);
				if (!dag.getK(child).isEmpty()) { // if we closed a loop
					gatherLoopStates(parent, child);
					if (marked.isEmpty()) { // if there is no marked state then the loop is weak
						propagate(Status.WEAK, child, recommendation.getAction(), parent);
					} else {
						gatherUnsettledStates();
						if (unsettled.isEmpty()) // no unsettled means the loop is a controllably connected component
							propagateGoalFromMarked();
						else
							reopenUnsettled();
					}
					clearLoopDetection();
				} else { // otherwise we keep exploring
					if (child.getTransitions().isEmpty()) { // a deadlock
						setError(child);
						propagate(Status.ERROR, child, recommendation.getAction(), parent);
					} else if (!child.isLive()) {
						if (!child.isEvaluated())
							abstraction.eval(child);
						if (child.hasValidRecommendation()) {
							child.open();
						} else if (!child.hasStatusChild(Status.NONE)) {
							setError(child);
							propagate(Status.ERROR, child, recommendation.getAction(), parent);
						}
					}
				}
			}
		}
		
	}
	
	
	/** Clears internal data used for efficient loop detection. */
	private void clearLoopDetection() {
		dag.clear();
		loop.clear();
		ccc.clear();
		marked.clear();
		unsettled.clear();
	}
	
	
	/** Returns a map representing a DAG from parent to child.
	 *  The child state in a terminal node indicates a loop. */
	private void buildAncestorsDAG(Compostate child, Compostate parent) {
		ancestorStates.add(child);
		visited.add(child);
		for (int i = 0; i < ancestorStates.size(); ++i) {
			Compostate state = ancestorStates.get(i);
			for (Pair<HAction,Compostate> predecesor : state.getParents()) {
				Compostate predState = predecesor.getSecond();
				if (predState.isStatus(Status.ERROR))
					continue;
				dag.put(state, predState);
				if (visited.add(predState))
					ancestorStates.add(predState);
			}
		}
		visited.clear();
		ancestorStates.clear();
	}
	
	
	/** Gathers the states enclosed by a loop over parent and child states.
	 *  Additionally gathers the marked states in the loop. */
	private void gatherLoopStates(Compostate parent, Compostate child) {
		ancestorStates.add(child);
		for (int i = 0; i < ancestorStates.size(); ++i) {
			Compostate state = ancestorStates.get(i);
			for (Compostate s : dag.getK(state)) {
				if (s.marked)
					marked.add(s);
				if (loop.add(s))
					ancestorStates.add(s);
			}
		}
		ancestorStates.clear();
		if (!marked.isEmpty() && loop.contains(child))
			gatherCCC(parent, child);
		else
			marked.clear();
	}
	
	
	/** Gathers the states in a CCC. */
	private void gatherCCC(Compostate parent, Compostate child) {
		for (Compostate state : loop) {
			ccc.add(state);
			ancestorStates.add(state);
		}
		for (int i = 0; i < ancestorStates.size(); ++i) {
			Compostate state = ancestorStates.get(i);
			for (Compostate s : dag.getV(state)) {
				if (ccc.add(s))
					ancestorStates.add(s);
			}
		}
		ancestorStates.clear();		
	}
	
	
	/** Gathers unsettled states in the detected loop. */
	private void gatherUnsettledStates() {
		for (Compostate s : ccc)
			if (!s.isSettled())
				unsettled.add(s);
	}

	
	/** Triggers the propagation of a goal from marked states found. */
	private void propagateGoalFromMarked() {
		for (Compostate state : marked) {
			setGoal(state, 0);
			visited.add(state);
			addAncestors(state, Status.STRONG);
		}
		propagate();
	}
	
	
	/** Reopens unsettled state for further exploration. */
	private void reopenUnsettled() {
		for (Compostate state : unsettled) {
			if (!state.inOpen && state.hasValidRecommendation())
				state.open();
		}
	}
	
	
	/** Given an ancestor relation (action,parent) from a child known 
	 *  this method propagates a status information back to its ancestors.
	 *  @see propagate() */
	private void propagate(Status status, Compostate child, HAction action, Compostate parent) {
		BinaryRelation<HAction, Compostate> heir = new BinaryRelationImpl<>();
		heir.addPair(action, parent);
		ancestorTransitions.add(Pair.create(status, heir));
		if (status == Status.ERROR)
			child.removeParent(action, parent);
		propagate();
	}
	
	
	/** Given an ancestor relation in the ancestors internal list from a
	 *  state this method propagates this information back. */
	@SuppressWarnings("incomplete-switch")
	private void propagate() {
		while (!ancestorTransitions.isEmpty()) {
			Pair<Status,BinaryRelation<HAction,Compostate>> pair = ancestorTransitions.pop();
			Status status = pair.getFirst();
			BinaryRelation<HAction,Compostate> parents = pair.getSecond();
			propagation: for (Pair<HAction,Compostate> predecesor : parents) {
				HAction predAction = predecesor.getFirst();
				Compostate predState = predecesor.getSecond();
				if (predState.isStatus(Status.ERROR))
					continue propagation;
				if (!nonblocking && status == Status.WEAK)
					status = Status.ERROR; // @see Compostate.clear(), we allow reopening a weak state marked as error to ensure correctness.
				switch (status) {
					case ERROR:
						if (predAction.isControllable()) {
							if (predState.hasValidRecommendation()) {
								predState.open();
							} else if (predState.hasStatusChild(Status.STRONG)) {
								if (visited.add(predState))
									addAncestors(predState, Status.STRONG);
							} else if (predState.hasStatusChild(Status.WEAK)) {
								setUnsettled(predState);
								if (visited.add(predState))
									addAncestors(predState, Status.WEAK);
							} else if (!predState.hasLiveChild()) {
								setError(predState);
								if (visited.add(predState))
									addAncestors(predState, Status.ERROR);
								predState.clear();
							}
						} else {
							setError(predState);
							if (visited.add(predState))
								addAncestors(predState, Status.ERROR);
							predState.clear();
						}
						break;
					case STRONG:
						if (predState.hasValidUncontrollableRecommendation()) {
							predState.open();
						} else if (!predState.hasLiveChild()) {
							setGoal(predState, predAction);
							if (visited.add(predState))
								addAncestors(predState, Status.STRONG);
						}
						break;
					case WEAK:
						boolean hasValidRecommendation = predState.hasValidRecommendation();
						boolean uncontrollableRecommendation = hasValidRecommendation && predState.hasValidUncontrollableRecommendation();
						boolean hasGoalChild = predState.hasStatusChild(Status.STRONG);
						if (hasValidRecommendation && (uncontrollableRecommendation || !hasGoalChild))
							predState.open();
						else if (!hasValidRecommendation || !uncontrollableRecommendation) {
							if (!predState.hasLiveChild()) {
								if (hasGoalChild) {
									setGoal(predState, predAction);
									if (visited.add(predState))
										addAncestors(predState, Status.STRONG);
								} else {
									setUnsettled(predState);
									if (visited.add(predState))
										addAncestors(predState, Status.WEAK);
								}
							}
						}
						break;
				}
			}
		}
		visited.clear();
	}
	
	
	/** Adds the parents of a given state to the ancestor list (used during
	 *  goal and error propagation). */
	private void addAncestors(Compostate compostate, Status status) {
		BinaryRelation<HAction,Compostate> parents = compostate.getParents();
		if (parents != null)
			ancestorTransitions.add(Pair.create(status, parents));
	}
	
	
	/** Returns whether a given state is an error or not. */
	private boolean isError(Compostate compostate) {
		return compostate.isStatus(Status.ERROR) || compostate.getStates().contains(-1L);
	}
	
	
	/** Marks a given state as an error. */
	private void setError(Compostate compostate) {
		compostate.setStatus(Status.ERROR);
		setFinal(compostate);
	}
	
	
	/** Returns whether a given state is a goal or not. */
	private boolean isGoal(Compostate compostate) {
		return compostate.isStatus(Status.STRONG);
	}
	

	/** Marks a given state as a goal. */
	private void setGoal(Compostate compostate, HAction action) {
		int distance = compostate.getChildDistance(action);
		if (distance < INF)
			distance++;
		setGoal(compostate, distance);
	}
	
	
	/** Marks a given state as a goal. */
	private void setGoal(Compostate compostate, int distance) {
		compostate.setStatus(Status.STRONG);
		if (distance < compostate.getDistance())
			compostate.setDistance(distance);
		setFinal(compostate);
	}
	
	
	/** Marks a given state as unsettled. */
	private void setUnsettled(Compostate compostate) {
		//compostate.setStatus(Status.WEAK);
		//setFinal(compostate);
	}
	
	
	/** Returns whether an state is unsettled or not. */
	private boolean isUnsettled(Compostate compostate) {
		return compostate.isStatus(Status.WEAK);
	}
	
	
	/** Marks a given state as final, closing it and releasing some resources. */
	private void setFinal(Compostate compostate) {
		compostate.close();
		compostate.clearRecommendations();
	}
	
	
	/** Removes events leading to unsettled states that do not close a loop.
	 *  This might uncover that the problem has no solution. */
	private void filterUnsettled() {
		descendants.add(initial);
		Set<Compostate> visited = new HashSet<>();
		visited.add(initial);
		while (!descendants.isEmpty()) {
			Compostate parent = descendants.remove();
			for (Pair<HAction,Compostate> transition : parent.getChildren()) {
				Compostate child = transition.getSecond();
				if (isUnsettled(child)) {
					if (nonblocking) // if strong discard weak states (this requires to greedily go for strong goals).
						buildAncestorsDAG(child, parent);
					if (!dag.keySet().contains(child)) // !isAncestor(child, parent)
						propagate(Status.ERROR, child, transition.getFirst(), parent);
					clearLoopDetection();
				}
				else if (child.getStatus() != Status.NONE && visited.add(child))
					descendants.add(child);
			}
		}
	}
	
	
	/** After the synthesis procedure this method builds a controller in the 
	 *  form of an LTS by starting in the initial state and following all the
	 *  non-closed descendants. If there is no controller for the given
	 *  environment this method returns null. */
	private LTS<Long, Action> buildController() {
		if (!isGoal(initial))
			return null;
		filterUnsettled();
		if (isError(initial))
			return null;
		long i = 0;
		LTSImpl<Long, Action> result = new LTSImpl<Long, Action>(i);
		Map<Compostate, Long> ids = new HashMap<>();
		ids.put(initial, i++);
		descendants.add(initial);
		result.addActions(alphabet.getActions()); // adds all the actions not just the reachable part
		Set<Pair<HAction,Compostate>> transitions = new HashSet<>();
		while (!descendants.isEmpty()) {
			Compostate successor = descendants.remove();
			BinaryRelationImpl.RelPair<HAction,Compostate> best =
				new BinaryRelationImpl.RelPair<>();
			int distance = INF;
			Status goal = Status.WEAK;
			for (Pair<HAction,Compostate> transition : successor.getChildren()) {
				Compostate child = transition.getSecond();
				if (!isGoal(child) && !isUnsettled(child))
					continue;
				int update = transition.getSecond().getDistance();
				Status status = successor.getStatus();
				if (goal.precedence > status.precedence || distance >= update) {
					goal = status;
					distance = update;
					best.copy(transition);
				}
			}
			if (successor.isControlled() && successor.getDistance() != 1) {
				if (best.getFirst() != null) {
					if (best.getSecond().isStatus(Status.WEAK)) { // if best is WEAK we add all transitions with the best estimate
						for (Pair<HAction,Compostate> transition : successor.getChildren()) {
							int transitionDistance = transition.getSecond().getDistance();
							Status status = successor.getStatus();
							if (status == Status.WEAK && transitionDistance == distance)
								transitions.add(transition);
						}
					} else {
						transitions.add(best); // only add the best controllable action if STRONG
					}
				}
			} else {
				for (Pair<HAction, Compostate> transition : successor.getChildren()) { // add all uncontrollable actions
					HAction action = transition.getFirst();
					Compostate child = transition.getSecond();
					if (child.isStatus(Status.NONE) || isError(child) || (!nonblocking && child.isStatus(Status.WEAK))) {
						if (!action.isControllable())
							return null;
						else
							continue;
					}
					if (!action.isControllable() || transition.equals(best))
						transitions.add(new Pair<>(action, child));
				}
			}
			if (!reachability && transitions.isEmpty()) // with a liveness goal I should never add a deadlock to the solution...
				return null;
			for (Pair<HAction,Compostate> transition : transitions) {
				Action action = transition.getFirst().getAction();
				Compostate child = transition.getSecond();
				if (!ids.containsKey(child)) {
					ids.put(child, i++);
					descendants.add(child);
					result.addState(ids.get(child));
				}
				//result.addAction(action); // I am already adding all the alphabet and not just the reachable part
				result.addTransition(ids.get(successor), action, ids.get(child));
			}
			transitions.clear();
		}
		statistics.setUsed(result.getStates().size());
		return result;
	}
	
	
	
	/** This class represents the distance from a state to a goal. */
	private static class HDist extends OrdPair<Integer, Integer> {

		/** This value represents an infinite distance. */
		public static HDist chasm = new HDist(2, INF);
		
		/** This value represents zero distance. */
		public static HDist zero = new HDist(0, 0);
		
		/** Factory for infinite values. */
		public static Factory<HDist> chasmFactory = new Factory<HDist>() {
			public HDist newInstance() { return HDist.chasm; }
		};
		
		/** Constructor for the HDist class. */
		public HDist(Integer first, Integer second) {
			super(first, second == null ? INF : second);
		}
		
		/** This method returns a new HDist with a one step greater distance. */
		public HDist inc() {
			return second == INF ? this : new HDist(first, second + 1);
		}
		
	}
	
	
	
	/** This class represents an heuristic estimate of the distance from a state to a goal. */
	private class HEstimate implements Comparable<HEstimate>, Cloneable {
		
		/** List of distances considered in this heuristic estimate. */
		private List<HDist> values;
		
		/** Constructor for an HEstimate. */
		public HEstimate(int capacity) {
			values = new ArrayList<>(capacity);
		}
		
		/** Constructor for an initialized HEstimate. */
		public HEstimate(int size, HDist init) {
			this(size);
			for (int i = 0; i < size; ++i)
				values.add(init);
		}
		
		/** Sorts this estimate in descending order. */
		public void sortDescending() {
			sort(values);
			reverse(values);
		}
		
		/** Compares to estimates using a lexicographical comparison. */
		@Override
		public int compareTo(HEstimate o) {
			int result = 0;
			for (int i = 0; result == 0 && i < values.size(); ++i)
				result = get(i).compareTo(o.get(i));
			return result;
		}
		
		/** Returns the distance at the i-th index. */
		public HDist get(int i) {
			return values.get(i);
		}
		
		/** Sets the distance for the i-th index. */
		public boolean set(int i, HDist d) {
			boolean result;
			if (result = (get(i).compareTo(d) > 0))
				values.set(i, d);
			return result;
		}
		
		/** Reduces this estimate to its maximum value. */
		public void reduceMax() {
			HDist max = get(0);
			for (int i = 1; i < values.size(); ++i) {
				if (get(i).compareTo(max) > 0)
					max = get(i);
			}
			values.clear();
			values.add(max);
		}
		
		/** Indicates whether this estimate represents a conflict. */
		public boolean isConflict() {
			return get(0).getSecond().equals(INF);
		}
		
		/** Returns whether two HEstimates are equals. */
		@SuppressWarnings("unchecked")
		@Override
		public boolean equals(Object obj) {
			boolean result = false;
			if (obj instanceof DirectedControllerSynthesis.HEstimate)
				result = this.compareTo((HEstimate)obj) == 0;
			return result;
		}
		
		/** Returns the hash code for an HEstimate. */
		@Override
		public int hashCode() {
			return values.hashCode();
		}

		/** Returns a copy of this object. */
		@Override
		public Object clone() {
			HEstimate result = new HEstimate(values.size());
			result.copy(this);
			return result;
		}
		
		/** Copies the values of an estimate into this estimate. */
		public void copy(HEstimate e) {
			values.clear();
			values.addAll(e.values);
		}
		
		/** Returns the string representation of this estimate. */
		@Override
		public String toString() {
			return values.toString();
		}
		
	}
	
	
	
	/** Builds (or retrieves from cache) an heuristic state. */
	private HState buildHState(int lts, State state) {
		HashMap<State, Integer> table = cache.get(lts); 
		if (table == null) {
			table = new HashMap<State, Integer>();
			cache.put(lts, table);
		}
		Integer index = table.get(state);
		if (index == null) {
			HState hstate = new HState(lts, state);
			index = hstate.hashCode();
			table.put(state, index);
		}
		return stash.get(index);
	}
	
	
	
	/** This class represents an Heuristic State, that is a state in the
	 *  abstraction that is used to estimate the distance to the goal. */
	private class HState {
		
		/** The id of the LTS to which this state belongs. */
		private final int lts;
		
		/** The original state this represents in the abstraction. */
		private final State state;
		
		/** Precomputed hash code. */
		private final int hash;
		
		/** Flags if this state represents a marked state. */
		private final boolean marked;
		
		
		/** Constructor for an Heuristic State. */
		public HState(int lts, State state) {
			this.lts = lts;
			this.state = state;
			this.hash = stash.size();
			this.marked = defaultTargets.get(lts).contains(state);
			stash.add(this);
		}
		
		
		/** Returns the outgoing transitions from this state in its own LTS. */
		public BinaryRelation<Action,State> getTransitions() {
			return ltss.get(lts).getTransitions(state); // accessing LTSs
		}
		
		
		///** Returns whether this state represents a marked state. */
		//public boolean isMarked() { // removed during aggressive inlining
		//	return marked;
		//}
		
		
		/** Returns if this state has a transition with the given action. */
		public boolean contains(HAction action) {
			Set<State> targets = getTransitions().getImage(action.getAction());
			return !(targets == null || targets.isEmpty());
		}
		
		
		/** Returns if this state has a self-loop transition with the given action. */
		public boolean isSelfLoop(HAction action) {
			return getTransitions().getImage(action.getAction()).contains(this.state); 
		}
		
		
		/** The hash code for this state. */
		@Override
		public int hashCode() {
			return hash;
		}
		
		
		/** Returns whether this state is equals to a given object. */
		@Override
		public boolean equals(Object obj) {
			boolean result = false;
			if (obj != null && obj instanceof DirectedControllerSynthesis.HState) {
				@SuppressWarnings("unchecked")
				HState other = (HState)obj;
				result = this.lts == other.lts &&
						 this.state.equals(other.state);
			}
			return result;
		}
		
		
		/** Returns the string representation of this state. */
		@Override
		public String toString() {
			return state.toString();
		}
		
	}

	

	/** This enum lists the available abstractions. */
	public enum AbstractionMode {
		Monotonic, Ready, Dummy;
	}
	
	
	
	/** Abstract class for abstractions. */
	private abstract class Abstraction {
		public abstract void eval(Compostate compostate);
	}
	
	
	
	/** This class implements the Monotonic Abstraction (MA). */
	private class MonotonicAbstraction extends Abstraction {

		/** Set of enabled events (ready in each LTS). */
		private TransitionSet allReady;
		
		/** Set of fresh states discovered during an iteration. */
		private Set<HState> freshStates;
		
		/** Set of fresh actions discovered during an iteration. */
		private Set<HAction> freshActions;
		
		/** Mapping of actions to the generation index in which they were first included. */
		private Map<HAction, Integer> actionGenerations;
		
		/** Mapping of states to the generation index in which they were first included. */
		private Map<HState, Integer> stateGenerations;
		
		/** Mapping of pending transitions, that is, not yet enabled event that lead to different states. */
		private Map<HAction, Set<HState>> pending;
		
		/** List of selected LTSs to evaluate. */
		private List<Integer> selected;
		
		/** Mapping of actions to estimates (the result of evaluating the abstraction). */
		private Map<HAction, HEstimate> estimates;
		
		/** Mapping of states to the reachable states and the actions leading to them plus in how many steps. */
		private Map<HState, Map<HState, Map<HAction, Integer>>> paths;
		
		
		/** Constructor for the MA. */
		public MonotonicAbstraction() {
			freshStates = new HashSet<>();
			freshActions = new HashSet<>();
			actionGenerations = new HashMap<>();
			stateGenerations = new HashMap<>();
			pending = new InitMap<>(HashSet.class);
			selected = new ArrayList<>(ltssSize);
			estimates = new InitMap<>(new Factory<HEstimate>() {
				@Override
				public HEstimate newInstance() {
					return new HEstimate(ltssSize, HDist.chasm);
				}
			});
			paths = new InitMap<>(HashMap.class);
		}
		
		
		/** Clears the interal data kept by the MA. */
		public void clear() {
			allReady = base.clone();
			freshStates.clear();
			actionGenerations.clear();
			stateGenerations.clear();
			pending.clear();
			selected.clear();
			estimates.clear();
			paths.clear();
		}
		
		
		/** Performs the heuristic evaluation by building the MA. */
		@Override
		public void eval(Compostate compostate) {
			if (!compostate.isEvaluated()) {
				clear();
				buildMA(compostate);
				evaluateMA(compostate);
				extractRecommendations(compostate);
			}
		}
		
		
		/** Build the MA from a given state. */
		public void buildMA(Compostate compostate) {
			for (int lts = 0; lts < ltssSize; ++lts)
				freshStates.add(buildHState(lts, compostate.getStates().get(lts)));
			int iteration = 0;
			update(iteration);
			while (!freshStates.isEmpty()) {
				++iteration;
				freshStates.clear();
				for (HAction action : freshActions) {
					for (HState state : pending.get(action)) {
						if (!stateGenerations.containsKey(state))
							freshStates.add(state);
					}
					pending.get(action).clear();
				}
				freshActions.clear();
				update(iteration);
			}
		}
		
		
		/** Updates the fresh states for the given iteration. */
		private void update(int iteration) {
			for (HState s : freshStates) {
				stateGenerations.put(s, iteration);
				LTS<State, Action> current = ltss.get(s.lts);
				for (Pair<Action,State> transition : current.getTransitions(s.state)) {
					HState d = buildHState(s.lts, transition.getSecond());
					HAction l = alphabet.getHAction(transition.getFirst());
					pending.get(l).add(d);
					allReady.add(s.lts, l);
					if (!actionGenerations.containsKey(l) && allReady.contains(l)) {
						actionGenerations.put(l, iteration);
						freshActions.add(l);
					}
				}
			}
		}
		
		
		/** Evaluates the MA populating the estimates table. */
		public void evaluateMA(Compostate compostate) {
			estimates.clear();
			
			boolean targeted = select(compostate);
			if (selected.isEmpty())
				return;
			
			for (Integer lts : selected) {
				HState s = buildHState(lts, compostate.getStates().get(lts));
				Map<HState, Map<HAction, Integer>> pathsFromSource = paths.get(s);
				for (HState t : pathsFromSource.keySet()) {
					if (!t.marked/*isMarked()*/)
						continue;
					Map<HAction, Integer> pathsToTarget = pathsFromSource.get(t);
					for (Entry<HAction, Integer> entry : pathsToTarget.entrySet()) {
						HAction l = entry.getKey();
						Integer d = entry.getValue();
						Integer m = targeted && compostate.getTargets(lts).contains(t.state) ? 0 : 1;
						HDist newDist = new HDist(m, d);
						HDist lDist = estimates.get(l).get(lts);
						if (newDist.compareTo(lDist) < 0)
							estimates.get(l).set(lts, newDist);
					}
				}
			}
			
			reconcilateCrossLTS(compostate);
		}
		
		
		/** Selects the LTSs to consider in the MA. */
		private boolean select(Compostate compostate) {
			boolean includeTargets = true;
			computePaths();
			for (int lts = 0; lts < ltssSize; ++lts) {
				HState state = buildHState(lts, compostate.getStates().get(lts));
				boolean isTarget = false, isMarked = false;
				for (HState target : paths.get(state).keySet()) {
					isMarked |= target.marked/*isMarked()*/;
					isTarget |= compostate.getTargets(lts).contains(target.state);
				}
				if (isTarget) {
					if (includeTargets)
						selected.add(lts);
				} else if (isMarked) {
					if (includeTargets) {
						selected.clear();
						includeTargets = false;
					}
					selected.add(lts);
				} else {
					selected.clear();
					break;
				}
			}
			return includeTargets;
		}
		

		/** Reconcilates the estimates for the LTS which have not been analyzed. */
		private void reconcilateCrossLTS(Compostate compostate) {
			for (int lts = 0; lts < ltssSize; ++lts) {
				HState s = buildHState(lts, compostate.getStates().get(lts));
				for (HAction l : compostate.getTransitions()) {
					if (!selected.contains(lts))
						estimates.get(l).set(lts, HDist.zero);
					if (!ltss.get(lts).getActions().contains(l.getAction()) || s.isSelfLoop(l)) {
						HDist lDist = estimates.get(l).get(lts);
						if (lDist.compareTo(HDist.chasm) == 0)
							estimates.get(l).set(lts, HDist.zero);
					}
				}
			}
		}
		
		
		/** Extracts recommendations for the given state using the computed estimates. */
		private void extractRecommendations(Compostate compostate) {
			compostate.setupRecommendations();
			for (HAction action : compostate.getTransitions()) {
				HEstimate estimate = estimates.get(action);
				estimate.reduceMax();
				if (compostate.addRecommendation(action, estimate))
					break;
			}
			compostate.rankRecommendations();
			compostate.initRecommendations();
		}
		
		
		/** Returns the estimated distance that a transition adds to a path. */
		private Integer dist(HState source, HAction label) {
			if (!stateGenerations.containsKey(source) || !actionGenerations.containsKey(label))
				return INF;
			return 1 + Math.max(0, actionGenerations.get(label) - stateGenerations.get(source));
		}
		
		
		/** Returns the minimum distance from the valid transitions from a given state. */
		private Integer minDist(HState source, Set<HAction> labels) {
			Integer result = INF;
			for (HAction action : labels)
				result = Math.min(result, dist(source, action));
			return result;
		}
		
		
		/** Adds to distances (maxing at overflows). */
		private Integer addDist(Integer d1, Integer d2) {
			if (d1 == INF || d2 == INF)
				return INF;
			return d1 + d2;
		}
		
		
		/** Computes the paths in the abstraction (the distance between states). */
		private void computePaths() {
			Map<HState, Map<HState, Set<HAction>>> oneStepReachableStates = new InitMap<>(HashMap.class);
			Map<HState, Set<Pair<HAction,HState>>> lastStates = new InitMap<>(HashSet.class);
			Map<HState, Set<Pair<HAction,HState>>> nextStates = new InitMap<>(HashSet.class);
			boolean statesPopulated = false;
			
			Map<HState, Map<HAction, Integer>> manyStepsReachableFromSource;
			
			for (int lts = 0; lts < ltssSize; ++lts) {
				for (State state : ltss.get(lts).getStates()) {
					HState source = buildHState(lts, state);
					manyStepsReachableFromSource = paths.get(source);
					Map<HState, Set<HAction>> oneReachableFromSoruce = oneStepReachableStates.get(source);
					for (Pair<Action, State> transition : ltss.get(lts).getTransitions(state)) {
						HAction label = alphabet.getHAction(transition.getFirst());
						HState destination = buildHState(lts, transition.getSecond());
						Set<HAction> oneStepToDestination = oneReachableFromSoruce.get(destination);
						Map<HAction, Integer> manyStepsToDestination = manyStepsReachableFromSource.get(destination);
						if (oneStepToDestination == null) {
							oneReachableFromSoruce.put(destination, oneStepToDestination = new HashSet<>());
							manyStepsReachableFromSource.put(destination, manyStepsToDestination = new HashMap<>());
						}
						Integer newDist = dist(source, label);
						if (newDist != INF) { //!!!
							putmin(manyStepsToDestination, label, newDist);
							oneStepToDestination.add(label);
							statesPopulated |= lastStates.get(source).add(Pair.create(label, destination));
						}
					}
				}
			}
			
			while (statesPopulated) {
				statesPopulated = false;
				for (HState source : lastStates.keySet()) {
					manyStepsReachableFromSource = paths.get(source);
					for (Pair<HAction, HState> pair : lastStates.get(source)) {
						HAction label = pair.getFirst();
						HState intermediate = pair.getSecond();
						Integer distIntermediate = manyStepsReachableFromSource.get(intermediate).get(label);
						for (HState target : oneStepReachableStates.get(intermediate).keySet()) {
							Map<HAction, Integer> manyStepsReachableFromSourceToTarget = manyStepsReachableFromSource.get(target);
							if (manyStepsReachableFromSourceToTarget == null)
								manyStepsReachableFromSource.put(target, manyStepsReachableFromSourceToTarget = new HashMap<>());
							Integer newDist = addDist(distIntermediate, minDist(intermediate, oneStepReachableStates.get(intermediate).get(target)));
							if (newDist != INF) { //!!!
								Integer current = manyStepsReachableFromSourceToTarget.get(label);
								if (current == null || current > newDist) {
									manyStepsReachableFromSourceToTarget.put(label, newDist);
									statesPopulated |= nextStates.get(source).add(Pair.create(label, target));
								}
							}
						}
					}
				}
				for (Set<Pair<HAction,HState>> set : lastStates.values())
					set.clear();
				Map<HState, Set<Pair<HAction,HState>>> swap = lastStates;
				lastStates = nextStates;
				nextStates = swap;
			}
		}
		
	}
	
	
	/** This class implements the Ready Abstraction (RA). */
	private class ReadyAbstraction extends Abstraction {

		/** Set of vertices in the RA graph. */
		private Set<HAction> vertices;
		
		/** Edges in the RA graph. */
		private BidirectionalMap<HAction, HAction> edges;
		
		/** Mapping of estimates for actions (the result of evaluating the abstraction). */
		private Map<HAction, HEstimate> estimates;

		/** The minimum estimate per LTS. */
		private Map<Integer, HDist> shortest;
		
		/** Fresh states discovered at each iteration. */
		private QueueSet<HAction> fresh;
		
		private Map<HAction, Set<Integer>> readyInLTS;
		
		/** Mapping of actions to LTSs containing them in their alphabets. */
		private Map<HAction, Set<Integer>> actionsToLTS;
		
		/** Maps for each state which other states can be reached.
		 *  It also stores which actions lead from source to destination and in how many local steps. */
		private Map<HState, Map<HState, Map<HAction, Integer>>> manyStepsReachableStates;
		
		/** Subset of the manyStepsReachableState map, containing only which
		 *  marked states can be reached from a given source. This map keeps
		 *  aliasing/sharing with the other and it is used only to speed up
		 *  iteration while looking for marked states to reach. */
		private Map<HState, Map<HState, Map<HAction, Integer>>> markedReachableStates;
		
		/** Maps for each state which actions can be reached.
		 *  It also stores which actions lead from source to desired action and in how many local steps. */
		private Map<HState, Map<HAction, Map<HAction, Integer>>> manyStepsReachableActions;
		
		/** Cache of target distances. */
		private Map<Integer, HDist> m0Cache;
		
		/** Cache of marked distances. */
		private Map<Integer, HDist> m1Cache;
		
		/** Cache of gaps between actions. */
		private Map<HAction, Map<HAction, Integer>> gapCache;
		
		
		/** Constructor for the RA. */
		public ReadyAbstraction() {
			vertices = new HashSet<>();
			edges = new BidirectionalMap<>();
			estimates = new InitMap<>(new Factory<HEstimate>() {
				@Override
				public DirectedControllerSynthesis<State, Action>.HEstimate newInstance() {
					HEstimate result = new HEstimate(ltssSize+1, HDist.chasm);
					result.values.remove(ltssSize);
					return result;
				}
			});
			shortest = new InitMap<>(HDist.chasmFactory);
			fresh = new QueueSet<>();
			actionsToLTS = new InitMap<>(HashSet.class);
			readyInLTS = new InitMap<>(HashSet.class);
			manyStepsReachableStates = new InitMap<>(HashMap.class);
			markedReachableStates = new InitMap<>(HashMap.class);
			manyStepsReachableActions = new InitMap<>(HashMap.class);
			m0Cache = new HashMap<>();
			m1Cache = new HashMap<>();
			gapCache = new InitMap<>(HashMap.class);
			init();
		}
		
		
		/** Initializes the RA precomputing tables. */
		private void init() {
			computeActionsToLTS();
			computeReachableStates();
			computeMarkedReachableStates();
			computeReachableActions();	
		}
		
		
		/** Clears the RA internal state. */
		private void clear() {
			vertices.clear();
			edges.clear();
			estimates.clear();
			shortest.clear();
			fresh.clear();
			readyInLTS.clear();
			gapCache.clear();
		}
		
		
		/** Evaluates the abstraction by building and exploring the RA. */
		@Override
		public void eval(Compostate compostate) {
			if (!compostate.isEvaluated()) {
				clear();
				buildRA(compostate);
				evaluateRA(compostate);
				extractRecommendations(compostate);
			}
		}
		
		
		/** Builds the RA by connecting ready events through edges indicating their causal relationship. */
		private void buildRA(Compostate compostate) {
			for (int lts = 0; lts < ltssSize; ++lts) {
				HState s = buildHState(lts, compostate.getStates().get(lts));
				for (Pair<Action,State> transition : s.getTransitions()) {
					HAction action = alphabet.getHAction(transition.getFirst());
					if (!s.state.equals(transition.getSecond())) { // !s.isSelfLoop(action)
						readyInLTS.get(action).add(lts);
						vertices.add(action);
					}
				}
			}
			for (HAction t : vertices) {
				for (Integer lts : actionsToLTS.get(t)) {
					HState s = buildHState(lts, compostate.getStates().get(lts));
					Map<HAction, Integer> actionsLeadingToTfromS = manyStepsReachableActions.get(s).get(t);
					if (actionsLeadingToTfromS != null) {
						for (HAction l : actionsLeadingToTfromS.keySet()) {
							if (!l.equals(t) && s.contains(l) && !s.isSelfLoop(l)) { // we need an efficient s.contains(l) that returns false for self-loops
								edges.put(l, t);
							}
						}
					}
				}
			}
		}
		
		
		/** Evaluates the RA by exploring the graph and populating the estimates table. */
		private void evaluateRA(Compostate compostate) {
			for (int lts = 0; lts < ltssSize; ++lts) {
				HState s = buildHState(lts, compostate.getStates().get(lts));
				Set<State> markedStates = defaultTargets.get(lts);
				Set<State> targetStates = compostate.getTargets(lts);
				Map<HState, Map<HAction, Integer>> markedReachableStatesFromSource = markedReachableStates.get(s);
				for (Pair<Action,State> transitions : s.getTransitions()) {
					HAction l = alphabet.getHAction(transitions.getFirst());
					State t = transitions.getSecond();
					if (t.equals(-1L))
						continue;
					Integer mt = 2, dt = INF;
					if (markedStates.contains(t)) {
						mt = targetStates.contains(t) ? 0 : 1;
						dt = 1;
					}
					if (!(mt == 0 || (mt == 1 && targetStates.isEmpty()))) { // already best, skip search
						if (s.state.equals(t)) // a self-loop
							continue;
						for (HState g : markedReachableStatesFromSource.keySet()) { // search for best
							Integer mg = targetStates.contains(g.state) ? 0 : 1;
							Integer dg = markedReachableStatesFromSource.get(g).get(l);
							if (dg == null)
								continue;
							if (mg < mt || (mg == mt && dg < dt)) {
								mt = mg;
								dt = dg;
							}
						}
					}
					HDist newlDist = getHDist(mt, dt);
					HDist currentDist = estimates.get(l).get(lts);
					if (newlDist.compareTo(currentDist) < 0) {
						estimates.get(l).set(lts, newlDist);
						fresh.add(l);
						if (compostate.getTransitions().contains(l)) // register only the shortest distances for enabled events
							registerShort(lts, newlDist);
					}
				}
			}
			
			while (!fresh.isEmpty()) {
				HAction t = fresh.poll();
				for (HAction l : edges.getK(t)) {
					Integer dtl = gap(compostate, l, t);
					for (int lts = 0; lts < ltssSize; ++lts) {
						if (readyInLTS.get(l).contains(lts))
							continue;
						HDist tDist = estimates.get(t).get(lts);
						HDist lDist = estimates.get(l).get(lts);
						Integer dl = addDist(tDist.getSecond(), dtl);
						HDist newlDist = dl == INF ? HDist.chasm : getHDist(tDist.getFirst(), dl);
						if (newlDist.compareTo(lDist) < 0) {
							estimates.get(l).set(lts, newlDist);
							fresh.add(l);
							if (compostate.getTransitions().contains(l))
								registerShort(lts, newlDist);
						}							
					}
				}
				
			}

			reconcilateShort(compostate);
		}

		
		/** Returns a distance from cache. */
		private HDist getHDist(Integer m, Integer d) {
			Map<Integer, HDist> mCache = m == 0 ? m0Cache : m1Cache;
			HDist result = mCache.get(d);
			if (result == null)
				mCache.put(d, result = new HDist(m, d));
			return result;
		}
		
		
		/** Returns the maximum distance between two actions from the current state of every LTS. */
		private Integer gap(Compostate compostate, HAction l, HAction t) {
			Integer result = gapCache.get(l).get(t);
			if (result != null)
				return result;
			result = 0;
			int score = 0;
			for (Integer lts : actionsToLTS.get(l)) {
				if (!actionsToLTS.get(t).contains(lts))
					continue;
				HState s = buildHState(lts, compostate.getStates().get(lts));
				if (s.contains(l)) {
					Map<HAction, Integer> actionFromSourceToTarget = manyStepsReachableActions.get(s).get(t);
					Integer dl = actionFromSourceToTarget == null ? null : actionFromSourceToTarget.get(l);
					dl = dl == null ? INF : dl - 1;
					if (dl > result)
						result = dl;
				} else {
					score = 1;
				}
			}
			if (result != INF)
				result += score;
			gapCache.get(l).put(t, result);
			return result;
		}
		
		
		/** Adds to distances (maxing at overflows). */
		private Integer addDist(Integer d1, Integer d2) {
			return (d1 == INF || d2 == INF) ? INF : d1 + d2;
		}
		
		
		/** Registers a distance estimated for a given LTS if minimum. */
		private void registerShort(Integer lts, HDist dist) {
			HDist shortDist = shortest.get(lts);
			if (dist.compareTo(shortDist) < 0)
				shortest.put(lts, dist);
		}
		
		
		/** Reconciliates the distances for the LTSs for which an action has not been considered. */
		private void reconcilateShort(Compostate compostate) {
			for (int lts = 0; lts < ltssSize; ++lts) { // this loops sets any missing shortest information
				HDist shortLts = shortest.get(lts);
				if (shortLts == HDist.chasm) {
					HState s = buildHState(lts, compostate.getStates().get(lts));
					Map<HState, Map<HAction, Integer>> markedStatesReachableFroms = markedReachableStates.get(s);
					for (Entry<HState, Map<HAction, Integer>> entry : markedStatesReachableFroms.entrySet()) {
						HState t = entry.getKey();
						Integer m = compostate.getTargets(lts).contains(t.state) ? 0 : 1;
						if (m < shortLts.getFirst()) {
							for (Integer d : entry.getValue().values()) {
								if (d < shortLts.getSecond())
									shortLts = getHDist(m, d);
							}
						}
					}
					shortest.put(lts, shortLts);
				}
			}
			for (HAction l : compostate.getTransitions()) { // this loops fills missing goals with shortest paths
				HEstimate el = estimates.get(l);
				for (int lts = 0; lts < ltssSize; ++lts) {
					HState s = buildHState(lts, compostate.getStates().get(lts));
					if (readyInLTS.get(l).contains(lts) && !s.isSelfLoop(l))
						continue;
					HDist lDist = el.get(lts);
					if (lDist == HDist.chasm)
						el.set(lts, shortest.get(lts).inc());
				}
			}
		}
		
		
		/** Extracts recommendations for a state from the estimates table. */
		@SuppressWarnings("unchecked")
		private void extractRecommendations(Compostate compostate) {
			compostate.setupRecommendations();
			for (HAction action : compostate.getTransitions()) {
				HEstimate estimate = estimates.get(action);
				estimate.sortDescending();
				if (compostate.addRecommendation(action, estimate))
					break;
			}
			Recommendation first = compostate.rankRecommendations();
			if (first != null && !first.getEstimate().isConflict()) {
				HEstimate ef = (HEstimate)first.getEstimate().clone();
				HAction af = first.getAction();
				for (int i = 0; i < compostate.recommendations.size(); ++i) {
					Recommendation ri = compostate.recommendations.get(i);
					HEstimate ei = ri.getEstimate();
					if (!af.isControllable() && ri.getAction().isControllable()) {
						first = ri;
						ef = (HEstimate)first.getEstimate().clone();
						af = first.getAction();
					}
					if (!ri.getAction().isControllable())
						ei.values.add(0, getHDist(0,0));
					else if (ei.equals(ef))
						ei.values.add(0, getHDist(0,1));
					else if (!ei.isConflict())
						ei.values.add(0, getHDist(0,2));
					else
						ei.values.add(0, HDist.chasm);
				}
			}
			compostate.initRecommendations();
		}
		
		
		/** Populates the actionsToLTS map. */
		private void computeActionsToLTS() {
			for (int i = 0; i < ltssSize; ++i) {
				for (Action l : ltss.get(i).getActions()) {
					putadd(actionsToLTS, alphabet.getHAction(l), i);
				}
			}
		}
		
		
		/** Computes for each state in each LTS which other states can reach and in how many steps. */
		private Map<HState, Map<HState, Map<HAction, Integer>>> computeReachableStates() {
			Map<HState, Map<HState, Set<HAction>>> oneStepReachableStates = new InitMap<>(HashMap.class);
			Map<HState, Set<Pair<HAction,HState>>> lastStates = new InitMap<>(HashSet.class);
			Map<HState, Set<Pair<HAction,HState>>> nextStates = new InitMap<>(HashSet.class);
			boolean statesPopulated = false;
			
			Map<HState, Map<HAction, Integer>> manyStepsReachableFromSource;
			
			for (int lts = 0; lts < ltssSize; ++lts) { // this loop populates one step reachable states
				for (State state : ltss.get(lts).getStates()) {
					HState source = buildHState(lts, state);
					manyStepsReachableFromSource = manyStepsReachableStates.get(source);
					Map<HState, Set<HAction>> oneReachableFromSoruce = oneStepReachableStates.get(source);
					for (Pair<Action, State> transition : ltss.get(lts).getTransitions(state)) {
						HAction label = alphabet.getHAction(transition.getFirst());
						HState destination = buildHState(lts, transition.getSecond());
						Set<HAction> oneStepToDestination = oneReachableFromSoruce.get(destination);
						Map<HAction, Integer> manyStepsToDestination = manyStepsReachableFromSource.get(destination);
						if (oneStepToDestination == null) {
							oneReachableFromSoruce.put(destination, oneStepToDestination = new HashSet<>());
							manyStepsReachableFromSource.put(destination, manyStepsToDestination = new HashMap<>());
						}
						manyStepsToDestination.put(label, 1);
						oneStepToDestination.add(label);
						statesPopulated |= lastStates.get(source).add(Pair.create(label, destination));
					}
				}
			}
			
			int i = 2;
			while (statesPopulated) { // this loop extends the reachable states in the transitive closure (each iteration adds the states reachable in i steps).
				statesPopulated = false;
				for (HState source : lastStates.keySet()) {
					manyStepsReachableFromSource = manyStepsReachableStates.get(source);
					for (Pair<HAction, HState> pair : lastStates.get(source)) {
						HAction label = pair.getFirst();
						HState intermediate = pair.getSecond();
						for (HState target : oneStepReachableStates.get(intermediate).keySet()) {
							Map<HAction, Integer> manyStepsReachableFromSourceToTarget = manyStepsReachableFromSource.get(target);
							if (manyStepsReachableFromSourceToTarget == null)
								manyStepsReachableFromSource.put(target, manyStepsReachableFromSourceToTarget = new HashMap<>());
							Integer current = manyStepsReachableFromSourceToTarget.get(label);
							if (current == null) {
								manyStepsReachableFromSourceToTarget.put(label, i);
								statesPopulated |= nextStates.get(source).add(Pair.create(label, target));
							}
						}
					}
				}
				for (Set<Pair<HAction,HState>> set : lastStates.values())
					set.clear();
				Map<HState, Set<Pair<HAction,HState>>> swap = lastStates;
				lastStates = nextStates;
				nextStates = swap;
				++i;
			}
			
			return manyStepsReachableStates;
		}
		
		
		/** Computes for each state in each LTS which other *marked* states can reach and in how many steps. */
		private Map<HState, Map<HState, Map<HAction, Integer>>> computeMarkedReachableStates() {
			for (HState source : manyStepsReachableStates.keySet()) {
				Map<HState, Map<HAction, Integer>> markedStatesFromSource = markedReachableStates.get(source); 
				Map<HState, Map<HAction, Integer>> reachableStatesFromSource = manyStepsReachableStates.get(source);
				for (HState destination : reachableStatesFromSource.keySet()) {
					if (destination.marked/*isMarked()*/)
						markedStatesFromSource.put(destination, reachableStatesFromSource.get(destination));
				}
			}
			return markedReachableStates;
		}
		
		
		/** Computes for each state in each LTS which actions can be reached and in how many steps. */
		private Map<HState, Map<HAction, Map<HAction, Integer>>> computeReachableActions() {
			for (int lts = 0; lts < ltssSize; ++lts) {
				for (State state : ltss.get(lts).getStates()) { // this loop populates the reachable action with the LTSs' transition relations (one step)
					HState source = buildHState(lts, state);
					Map<HAction, Map<HAction, Integer>> reachableActionsFromSource = manyStepsReachableActions.get(source);
					for (Pair<Action,State> transition : source.getTransitions()) {
						HAction label = alphabet.getHAction(transition.getFirst());
						Map<HAction, Integer> reachableActionsThroughLabel = reachableActionsFromSource.get(label);
						if (reachableActionsThroughLabel == null)
							reachableActionsFromSource.put(label, reachableActionsThroughLabel = new HashMap<>());
						putmin(reachableActionsThroughLabel, label, 1);
					}
				}
				for (State state : ltss.get(lts).getStates()) { // this loop extends the reachable actions with the outgoing events from reachable states (many steps)
					HState source = buildHState(lts, state);
					Map<HState, Map<HAction, Integer>> statesReachableFromSource = manyStepsReachableStates.get(source);
					Map<HAction, Map<HAction, Integer>> actionsReachableFromSource = manyStepsReachableActions.get(source);
					for (HState destination : statesReachableFromSource.keySet()) {
						for (Entry<HAction, Integer> entry : statesReachableFromSource.get(destination).entrySet()) {
							HAction actionLeadingToDestination = entry.getKey();
							Integer distanceFromSourceToDestination = entry.getValue();
							for (Pair<Action,State> transition : destination.getTransitions()) {
								HAction target = alphabet.getHAction(transition.getFirst());
								Map<HAction, Integer> actionsLeadingToTarget = actionsReachableFromSource.get(target);
								if (actionsLeadingToTarget == null)
									actionsReachableFromSource.put(target, actionsLeadingToTarget = new HashMap<>());
								putmin(actionsLeadingToTarget, actionLeadingToDestination, distanceFromSourceToDestination + 1);
							}
						}
					}
				}
			}
			
			return manyStepsReachableActions;
		}
		
	}
	
	
	
	/** This class provides a Recommendation comparison that allows to create a
	 *  ranking of events. That is, a sorted list where first we have uncontrollable
	 *  events in descending order followed by controllable events in ascending order. */
	private class Ranker implements Comparator<Recommendation> {

		/** Compares two recommendations for ranking purposes.
		 *  Returns:
		 *   -1 if r1 is smaller than r2;
		 *    0 if r1 and r2 are equal; and
		 *    1 if r1 is greater than r2. */
		@Override
		public int compare(Recommendation r1, Recommendation r2) {
			int c1 = r1.getAction().isControllable() ? 1 : 0;
			int c2 = r2.getAction().isControllable() ? 1 : 0;
			int result = c1 - c2;
			if (result == 0)
				result = c1 == 1 ? r1.compareTo(r2) : r2.compareTo(r1);
			return result;
		}
		
	}
	
	
	
	/** This class represents a recommended course of action computed by the
	 *  heuristic procedure. */
	private class Recommendation implements Comparable<Recommendation> {

		/** The action recommended to explore. */
		private HAction action;
		
		/** The estimated distance to the goal. */
		private HEstimate estimate;
		
		/** Constructor for a recommendation. */
		public Recommendation(Compostate compostate, HAction action, HEstimate estimate) {
			this.action = action;
			this.estimate = estimate;
		}
		
		
		/** Returns the action this recommendation suggests. */
		public HAction getAction() {
			return action;
		}
		
		
		/** Returns the estimate distance to the goal for this recommendation. */
		public HEstimate getEstimate() {
			return estimate;
		}
		
		
		/** Compares two recommendations by (<=). */
		@Override
		public int compareTo(Recommendation o) {
			int result = estimate.compareTo(o.estimate);
			return result;
		}
		
		
		/** Returns the string representation of this recommendation. */
		@Override
		public String toString() {
			return action.toString() + estimate;
		}
		
	}
	
	
	
	/** This class represents a state in the parallel composition of the LTSs
	 *  in the environment. These states are used to build the fragment of the
	 *  environment required to reach the goal on-the-fly. The controller for
	 *  the given problem can then be extracted directly from the partial
	 *  construction achieved by using these states. */
	private class Compostate implements Comparable<Compostate> {
		
		/** States by each LTS in the environment that conform this state. */
		private final List<State> states; // Note: should be a set of lists for non-deterministic LTSs
		
		/** The estimated distance to the goal from this state. */
		private HEstimate estimate;
		
		/** Indicates whether this state is unsettled (2) a goal (1) or an error (-1) or not yet known (0). */
		private Status status;
		
		/** The real distance to the goal state from this state. */
		private int distance;
		
		/** Depth at which this state has been expanded. */
		private int depth;
		
		/** A ranking of the outgoing transitions from this state. */
		private List<Recommendation> recommendations;
		
		/** An iterator for the ranking of recommendations. */
		private Iterator<Recommendation> recommendit;
		
		/** Current recommendation (or null). */
		private Recommendation recommendation;
		
		/** Indicates whether the state is actively being used. */
		private boolean live;
		
		/** Indicates whether the state is in the open queue. */
		private boolean inOpen;
		
		/** Indicates whether the state is controlled or not. */
		private boolean controlled;

		/** Indicates whether the state is marked, that is, every contained state is marked. */
		private final boolean marked;
		
		/** Children states expanded following a recommendation of this state. */
		private final BinaryRelation<HAction, Compostate> children;
		
		/** Parents that expanded into this state. */
		private final BinaryRelation<HAction, Compostate> parents;
		
		/** Set of actions enabled from this state. */
		private final Set<HAction> transitions;
		
		/** Stores target states (i.e., already visited marked states) to reach from this state. */
		private List<Set<State>> targets = emptyList();

		
		/** Constructor for a Composed State. */
		public Compostate(List<State> states) {
			this.states = states;
			this.status = Status.NONE;
			this.distance = INF;
			this.depth = INF;
			this.live = false;
			this.inOpen = false;
			this.controlled = true; // we assume the state is controlled until an uncontrollable recommendation is obtained
			this.children = new BinaryRelationImpl<>();
			this.parents = new BinaryRelationImpl<>();
			boolean marked = true;
			for (int lts = 0; marked && lts < ltssSize; ++lts)
				marked &= defaultTargets.get(lts).contains(states.get(lts));
			this.marked = marked;
			this.transitions = buildTransitions();
		}
		
		
		/** Returns the states that conform this composed state. */
		public List<State> getStates() {
			return states;
		}
		
		
		/** Returns the distance from this state to the goal state (INF if not yet computed). */
		public int getDistance() {
			return distance;
		}
		
		
		/** Sets the distance from this state to the goal state. */
		public void setDistance(int distance) {
			this.distance = distance;
		}
		
		
		/** Returns the depth of this state in the exploration tree. */
		public int getDepth() {
			return depth;
		}
		
		
		/** Sets the depth for this state. */
		public void setDepth(int depth) {
			if (this.depth > depth)
				this.depth = depth;
		}
		
		
		/** Indicates whether this state has been evaluated, that is, if it has
		 *  a valid ranking of recommendations. */
		public boolean isEvaluated() {
			return recommendations != null;
		}
		
		
		///** Returns whether this state is marked (i.e., all its components are marked). */
		//public boolean isMarked() { // removed during aggressive inlining
		//	return marked;
		//}
		
		
		/** Returns the target states to be reached from this state as a list of sets,
		 *  which at the i-th position holds the set of target states of the i-th LTS. */
		public List<Set<State>> getTargets() {
			return targets;
		}
		
		
		/** Returns the target states of a given LTS to be reached from this state. */
		@SuppressWarnings("unchecked")
		public Set<State> getTargets(int lts) {
			return targets.isEmpty() ? (Set<State>)emptySet() : targets.get(lts);
		}
		
		
		/** Sets the given set as target states for this state (creates
		 *  aliasing with the argument set). */
		public void setTargets(List<Set<State>> targets) {
			this.targets = targets;
		}
		
		
		/** Adds a state to this state's targets. */
		public void addTargets(Compostate compostate) {
			List<State> states = compostate.getStates();
			if (targets.isEmpty()) {
				targets = new ArrayList<>(ltssSize);
				for (int lts = 0; lts < ltssSize; ++lts)
					targets.add(new HashSet<State>());
			}
			for (int lts = 0; lts < ltssSize; ++lts)
				targets.get(lts).add(states.get(lts));
		}
		
		
		/** Returns this state's status. */
		public Status getStatus() {
			return status;
		}
		
		
		/** Sets this state's status. */
		public void setStatus(Status status) {
			if (this.status != Status.ERROR || status == Status.ERROR)
				this.status = status;
		}
		
		
		/** Indicates whether this state's status equals some other status. */
		public boolean isStatus(Status status) {
			return this.status == status;
		}
		
		
		/** Sorts this state's recommendations in order to be iterated properly. */
		public Recommendation rankRecommendations() {
			Recommendation result = null;
			if (!recommendations.isEmpty()) {
				sort(recommendations, new Ranker());
				result = recommendations.get(0);
			}
			return result;
		}
		
		
		/** Sets up the recommendation list. */
		public void setupRecommendations() {
			if (recommendations == null)
				recommendations = new ArrayList<>();
		}
		
		
		/** Adds a new recommendation to this state and returns whether an
		 *  error action has been introduced (no other recommendations should
		 *  be added after an error is detected).
		 *  Recommendations should not be added after they have been sorted and
		 *  with an iterator in use. */
		public boolean addRecommendation(HAction action, HEstimate estimate) {
			boolean uncontrollableAction = !action.isControllable();
			if (controlled) { // may not work with lists of recommendations 
				if (uncontrollableAction)
					controlled = false;
			}
			if (!estimate.isConflict()) // update recommendation
				recommendations.add(new Recommendation(this, action, estimate));
			boolean error = uncontrollableAction && estimate.isConflict();
			if (error) // an uncontrollable state with at least one INF estimate is an automatic error
				recommendations.clear();
			return error;
		}
		
		
		/** Returns whether the iterator points to a valid recommendation. */
		public boolean hasValidRecommendation() {
			return /*isEvaluated()*/recommendations != null/**/ && recommendation != null; // replaced during aggressive inlining
		}
		
		
		/** Returns whether the iterator points to a valid uncontrollable recommendation. */
		public boolean hasValidUncontrollableRecommendation() {
			return /*hasValidRecommendation()*/recommendation != null/**/ && !recommendation.getAction().isControllable(); // replaced during aggressive inlining
		}
		
		
		/** Advances the iterator to the next recommendation. */
		public Recommendation nextRecommendation() {
			Recommendation result = recommendation;
			updateRecommendation();
			return result;
		}
		
		
		/** Initializes the recommendation iterator guaranteeing representation invariants. */
		public void initRecommendations() {
			recommendit = recommendations.iterator();
			updateRecommendation();
		}
		
		
		/** Initializes the recommendation iterator and current estimate for the state. */
		private void updateRecommendation() {
			if (recommendit.hasNext()) {
				recommendation = recommendit.next();
				estimate = recommendation.getEstimate(); // update this state estimate in case the state is reopened
			} else {
				recommendation = null;
			}
		}
		
		
		/** Clears all recommendations from this state. */
		public void clearRecommendations() {
			if (isEvaluated()) {
				recommendations.clear();
				recommendit = null;
				recommendation = null;
			}
		}
		
		
		/** Returns whether this state is being actively used. */
		public boolean isLive() {
			return live;
		}
		
		
		/** Returns whether this state has a child still open for exploration. */
		public boolean hasLiveChild() {
			int controllableGoals = 0;
			int controllableLive = 0;
			int uncontrollableLive = 0;
			for (Pair<HAction,Compostate> transition : getChildren()) {
				HAction action = transition.getFirst();
				Compostate child = transition.getSecond();
				if (child.isStatus(Status.ERROR))
					continue;
				if (child.isStatus(Status.STRONG) || child.isStatus(Status.WEAK) || ccc.contains(child)) {
					if (action.isControllable())
						controllableGoals++;
				} else {
					if (action.isControllable()) {
						controllableLive++;
					} else {
						uncontrollableLive++;
						break;
					}
				}
			}
			return uncontrollableLive > 0 || (controllableGoals == 0 && controllableLive > 0);
		}
		
		
		/** Returns whether this state has been settled (i.e., no live child or uncontrollable recommendations). */
		public boolean isSettled() {
			return !hasValidUncontrollableRecommendation() && !hasLiveChild();
		}
		
		
		/** Returns whether this state has a child with the given status. */
		public boolean hasStatusChild(Status status) {
			boolean result = false;
			for (Pair<HAction,Compostate> transition : getChildren()) {
				if (result = transition.getSecond().status == status)
					break;
			}
			return result;
		}
		
		
		/** Adds this state to the open queue (reopening it if was previously closed). */
		public boolean open() {
			boolean result = false;
			live = true;
			if (!inOpen) {
				if (inOpen = !hasStatusChild(Status.NONE)) {
					result = open.add(this);
				} else { // we are reopening a state, thus we reestablish it's children instead
					for (Pair<HAction,Compostate> transition : getChildren()) {
						Compostate child = transition.getSecond();
						if (!child.isLive() && child.status == Status.NONE && child.hasValidRecommendation()) // !isGoal(child)
							result |= child.open();
					}
					if (inOpen = (!result || isControlled()))
						result = open.add(this);
				}
			}
			return result;
		}
		
		
		/** Closes this state to avoid further exploration. */
		public void close() {
			live = false;
		}
		
		
		/** Returns whether this state is controllable or not. */
		public boolean isControlled() {
			return controlled;
		}
		
		
		/** Returns the set of actions enabled from this composed state. */
		public Set<HAction> getTransitions() {
			return transitions;
		}
		

		/** Initializes the set of actions enabled from this composed state. */
		private Set<HAction> buildTransitions() { // Note: here I can code the wia and ia behavior for non-deterministic ltss
			Set<HAction> result = new HashSet<>();
			if (facilitators == null) {
				for (int i = 0; i < states.size(); ++i) {
					for (Pair<Action,State> transition : ltss.get(i).getTransitions(states.get(i))) {
						HAction action = alphabet.getHAction(transition.getFirst());
						allowed.add(i, action);
					}
				}
			} else {
				for (int i = 0; i < states.size(); ++i)
					if (!facilitators.get(i).equals(states.get(i)))
						for (Pair<Action,State> transition : ltss.get(i).getTransitions(facilitators.get(i))) {
							HAction action = alphabet.getHAction(transition.getFirst());
							allowed.remove(i, action); // remove old non-shared facilitators transitions  
						}
				for (int i = 0; i < states.size(); ++i)
					if (!facilitators.get(i).equals(states.get(i)))
						for (Pair<Action,State> transition : ltss.get(i).getTransitions(states.get(i))) {
							HAction action = alphabet.getHAction(transition.getFirst());
							allowed.add(i, action); // add new non-shared facilitators transitions
						}
			}
			result.addAll(allowed.getEnabled());
			facilitators = states;
			return result;
		}
		
		
		/** Adds an expanded child to this state. */
		public void addChild(HAction action, Compostate child) {
			children.addPair(action, child);
		}
		
		
		/** Returns all transition leading to children of this state. */
		public BinaryRelation<HAction,Compostate> getChildren() {
			return children;
		}
		
		
		/** Returns the distance to the goal of a child of this compostate following a given action. */
		public int getChildDistance(HAction action) {
			int result = UNDEF; // children should never be empty or null
			for (Compostate compostate : children.getImage(action)) { // Note: maximum of non-deterministic children
				if (result < compostate.getDistance())
					result = compostate.getDistance();
			}
			return result;
		}
		
		
		/** Adds an expanded parent to this state. */
		public void addParent(HAction action, Compostate parent) {
			parents.addPair(action, parent);
			setDepth(parent.getDepth() + 1);
		}
		
		
		/** Removes a closed parent from this state. */
		public void removeParent(HAction action, Compostate parent) {
			parents.removePair(action, parent);
		}
		
		
		/** Returns the inverse transition leading to parents of this state. */
		public BinaryRelation<HAction,Compostate> getParents() {
			return parents;
		}
		
		
		/** Compares two composed states by their estimated distance to a goal by (<=). */
		@Override
		public int compareTo(Compostate o) {
			int result = estimate.compareTo(o.estimate);
			if (result == 0)
				result = this.depth - o.depth;
			return result;
		}
		
		
		/** Clears the internal state removing parent and children. */
		public void clear() {
			children.clear();
			if (!nonblocking) // this is a quick fix to allow reopening weak states marked as errors
				compostates.remove(states);
		}
		
		
		/** Returns the string representation of a composed state. */
		@Override
		public String toString() {
			return states.toString();
		}
		
	}
	
	
	
	/** This class encapsulates the alphabet of the plant.
	 *  Provides the required functionality for actions with perfect hashing.
	 *  @see HAction*/
	public class Alphabet implements Iterable<HAction> {
		
		/** Actions indexed by hash. */
		private List<Action> actions;
		
		/** HActions indexed by hash. */
		private List<HAction> hactions;
		
		/** Map of actions to their respective hash codes. */
		private Map<Action, HAction> codes;
		
		/** Controllable action flags indexed by hash. */
		private BitSet controlbits;
		
		
		/** Constructor for the Alphabet. */
		public Alphabet() {
			actions = new ArrayList<>();
			hactions = new ArrayList<>();
			codes = new HashMap<>();
			controlbits = new BitSet();
			alphabet = this;
			init();
		}
		
		
		/** Initialization. Populates the alphabet with the transitions from the LTSs. */
		private void init() {
			Set<Action> actions = new HashSet<>();
			for (int i = 0; i < ltssSize; ++i)
				actions.addAll(ltss.get(i).getActions());
			List<Action> labels = new ArrayList<>(actions.size());
			for (Action action : actions)
				labels.add(action);
			Collections.sort(labels, new Comparator<Action>() {
				public int compare(Action o1, Action o2) {
					return o1.toString().compareTo(o2.toString());
				}
			});
			for (Action label : labels) {
				HAction code = codes.get(label);
				if (code == null)
					codes.put(label, code = new HAction(label));
			}
		}
		
		
		/** Returns the size of the alphabet (the number of actions). */
		public int size() {
			return actions.size();
		}
		
		
		/** Provides an iterator for the actions in the alphabet. */
		@Override
		public Iterator<HAction> iterator() {
			return codes.values().iterator();
		}
		
		
		/** Given an action returns its perfectly hashed proxy. */
		public HAction getHAction(Action action) {
			return codes.get(action);
		}
		
		
		/** Returns a collection of all the actions that conform the alphabet. */
		public List<Action> getActions() {
			return actions;
		}
		
		
		/** Returns the list of HActions indexed by hash. */
		public List<HAction> getHActions() {
			return hactions;
		}
		
	}
	
	
	
	/** This class serves as a proxy for the Action type argument.
	 *  Internally it is represented simply by its hash value.
	 *  Together with the alphabet allows for a high-performance
	 *  representation of transitions. */
	public class HAction implements Comparable<HAction> {
		
		/** Hash value for this action. */
		private final int hash;
		
		
		/** Constructor for an HAction given a common Action. */
		public HAction(Action action) {
			hash = alphabet.actions.size();
			alphabet.actions.add(action);
			alphabet.hactions.add(this);
			if (controllable.isEmpty() || controllable.contains(action))
				alphabet.controlbits.set(hash);
		}
		
		
		/** Returns true if two HActions are equals. */
		@Override
		public boolean equals(Object obj) {
			boolean result = false;
			if (obj != null && obj instanceof DirectedControllerSynthesis.HAction) {
				@SuppressWarnings("unchecked")
				HAction other = (HAction)obj;
				result = this.hash == other.hash; 
			}
			return result;
		}
		
		
		/** Returns the hash code for this HAction. */
		@Override
		public int hashCode() {
			return hash;
		}
		
		
		/** Indicates whether this action is controllable. */
		public boolean isControllable() {
			return alphabet.controlbits.get(hash);
		}
		
		
		/** Returns the Action this HAction proxies. */
		public Action getAction() {
			return alphabet.actions.get(hash);
		}
		
		/** Return the String representation of this HAction. */
		@Override
		public String toString() {
			return getAction().toString();
		}


		/** Compares two actions. */
		@Override
		public int compareTo(HAction o) {
			return hash - o.hash;
		}
		
	}
	
	
	
	/** This class represents a set of synchronized transitions, that is to say, that
	 *  a transition belongs to the set if all intervening LTSs allow the action. */
	public class TransitionSet implements Cloneable {
		
		/** Maps actions to the number of LTSs enabling them. */
		private int[] enabledCount;
		
		/** The set of enabled transitions by each LTS. */
		private List<BitSet> enabledByLTS;
		
		/** The set of enabled transitions. */
		private Set<HAction> enabledActions;
		
		
		/** Constructor for a Transition Set (avoids initialization if alphabet is null). */
		public TransitionSet(Alphabet alphabet) {
			if (alphabet != null)
				init(alphabet);
		}
		
		
		/** Initialization for a Transition Set, marks as enabled for each LTS
		 *  all the actions that do not belong to its alphabet. */
		private void init(Alphabet alphabet) {
			enabledByLTS = new ArrayList<>(ltssSize);
			enabledCount = new int[alphabet.size()];
			enabledActions = new HashSet<>();
			for (int i = 0; i < ltssSize; ++i)
				enabledByLTS.add(new BitSet(alphabet.size()));
			for (HAction hAction : alphabet) {
				Action action = hAction.getAction();
				int hash = hAction.hashCode();
				int count = 0;
				for (int i = 0; i < ltssSize; ++i) {
					Set<Action> actionsOf_i = ltss.get(i).getActions();
					if (!actionsOf_i.contains(action)) {
						enabledByLTS.get(i).set(hash);
						count++;
					}
				}
				enabledCount[hash] = count;
				if (count == ltssSize)
					enabledActions.add(hAction);
			}
		}
		
		
		/** Adds an action enabled by the given LTS.
		 *  Returns true if the action was not enabled and it became enabled
		 *  as a consequence of this addition. */
		public boolean add(int lts, HAction action) {
			boolean result = false;
			int hash = action.hashCode();
			if (!enabledByLTS.get(lts).get(hash)) {
				enabledByLTS.get(lts).set(hash);
				int count = ++enabledCount[hash];
				result = count == ltssSize;
			}
			if (result)
				enabledActions.add(action);
			return result;
		}
		
		
		/** Removes an action enabled by the given LTS.
		 *  Returns true if the action was enabled and it became disabled
		 *  as a consequence of this removal. */
		public boolean remove(int lts, HAction action) {
			boolean result = false;
			int hash = action.hashCode();
			if (enabledByLTS.get(lts).get(hash)) {
				enabledByLTS.get(lts).clear(hash);
				int count = enabledCount[hash]--;
				result = count == ltssSize;
			}
			if (result)
				enabledActions.remove(action);
			return result;
		}
		
		
		/** Returns the set of enabled actions of this Transition Set. */
		public Set<HAction> getEnabled() {
			return enabledActions;
		}
		
		/** Returns whether an action belongs to this Transition Set.
		 *  That is, if all intervening LTSs allow the action. */
		public boolean contains(HAction action) {
			return enabledCount[action.hash/*hashCode()*/] == ltssSize; // replaced during aggressive inlining
		}
		
		
		/** Returns a new instance of a Transition Set by deep copying this. */
		@Override
		public TransitionSet clone() {
			TransitionSet result = new TransitionSet(null);
			result.enabledByLTS = new ArrayList<>(this.enabledByLTS.size());
			result.enabledActions = new HashSet<>(this.enabledActions);
			result.enabledCount = new int[this.enabledCount.length];
			System.arraycopy(this.enabledCount, 0, result.enabledCount, 0, this.enabledCount.length);
			for (int i = 0; i < this.enabledByLTS.size(); ++i) {
				BitSet enabledBy_i = new BitSet();
				enabledBy_i.or(enabledByLTS.get(i));
				result.enabledByLTS.add(enabledBy_i);
			}
			return result;
		}
		
		
		/** Overrides the internal representation of this Transition Set to
		 *  reflect that of another set. */
		public void copy(TransitionSet other) {
			enabledActions.clear();
			enabledActions.addAll(other.enabledActions);
			System.arraycopy(other.enabledCount, 0, this.enabledCount, 0, this.enabledCount.length);
			for (int i = 0; i < this.enabledByLTS.size(); ++i) {
				BitSet enabledBy_i = this.enabledByLTS.get(i);
				enabledBy_i.clear();
				enabledBy_i.or(other.enabledByLTS.get(i));
			}
		}
		
		
		/** Returns a string representation of this set. */
		@Override
		public String toString() {
			return enabledActions.toString();
		}
		
	}
	
	
	
	/** This class holds statistic information about the heuristic procedure. */
	public class Statistics {
		
		/** Number of expanded states. */
		private int expanded; // expanded is usually smaller than the iterations due to the reopening of states, we could also report the number of iterations
		
		/** Number of states in the generated controller. */
		private int used;
		
		/** System wall clock in milliseconds at the start of the procedure. */
		private long started;
		
		/** System wall clock in milliseconds at the end of the procedure. */
		private long ended;
		
		
		/** Increments the number of expended states. */
		public void incExpanded() {
			expanded++;
		}

		
		/** Sets the number of states used in the generated controller. */
		public void setUsed(int used) {
			this.used = used;
		}
		
		
		/** Marks the start of the procedure to measure elapsed time. */
		public void start() {
			started = System.currentTimeMillis();
		}

		
		/** Marks the end of the procedure to measure elapsed time. */
		public void end() {
			ended = System.currentTimeMillis();
		}
		
		
		/** Indicates if the heuristic procedure is running. */
		public boolean isRunning() {
			return ended == 0;
		}
		
		
		/** Returns the number of expanded states (so far). */
		public int getExpanded() {
			return expanded;
		}
		
		
		/** Returns the elapsed time from the start of the execution up to now. */
		public long getElapsed() {
			return System.currentTimeMillis() - started;
		}
		
		
		/** Returns an approximation of the memory used by the procedure. */
		public long getUsedMemory() {
			Runtime runtime = Runtime.getRuntime();
			return runtime.totalMemory() - runtime.freeMemory() /*- base*/;
		}
		
		
		/** Resets the statistics. */
		public void clear() {
			expanded = 0;
			used = 0;
			started = 0;
			ended = 0;
		} 
		
		
		/** Returns a string with the statistic data. */
		@Override
		public String toString() {
			return  "Expanded: " + expanded + "\n" +
					"Used: " + used + "\n" +
					"Elapsed: " + (ended - started) + " ms\n";
		}
		
		
		/** Returns a string with live statistics. */
		public String toLive() {
			return  "  " + expanded + " states expanded " +
					" (" + formatTime(getElapsed()) + ", " + formatMemory(getUsedMemory()) + ")";
		}
		
		
		/** Returns tab separated values with statistic data (expanded, evaluated, revisited, used, elapsed).*/
		public String toTSV() {
			return used + "\t" + (ended - started) + "\n";
		}

		
		/** Returns time in human readable format. */
		private String formatTime(long time) {
			long sec = 1000;
			long min = 60 * sec;
			long hour = 60 * min;
			String result = "";
			String space = "";
			if (time > hour) {
				result += (time / hour) + " h";
				time %= hour;
				space = " ";
			}
			if (time > min) {
				result += space + (time / min) + " min";
				time %= min;
				space = " ";
			}
			if (time > sec) {
				result += space + (time / sec) + " s";
				time %= sec;
			}
			return result;
		}
		
		
		/** Returns memory in human readable format. */
		private String formatMemory(long memory) {
			double kilo = 1024;
			double mega = 1024 * kilo;
			double giga = 1024 * mega;
			String result;
			if (memory > giga)
				result = String.format("%02.2f", memory / giga) + " GB";
			else if (memory > mega)
				result = String.format("%02.2f",memory / mega) + " MB";
			else if (memory > kilo)
				result = String.format("%02.2f",memory / kilo) + " KB";
			else
				result = memory + " Bs";
			return result;
		}
		
	}
	
	
	
	/** Auxiliary function to add a value to a set contained as value of a map. */
	private static <K,V> boolean putadd(Map<K, Set<V>> map, K key, V value) {
		Set<V> set = map.get(key);
		if (set == null)
			map.put(key, set = new HashSet<>());
		return set.add(value);
	}
	
	
	/** Auxiliary function to put an element into a map, but keeping the minimum if already set. */
	public static <K, V extends Comparable<V>> boolean putmin(Map<K, V> map, K key, V value) {
		boolean result;
		V old = map.get(key);
		if (result = (old == null || old.compareTo(value) > 0)) {
			map.put(key, value);
		}
		return result;
	}
	
	
	/** Auxiliary function to put an element into a map, but keeping the maximum if already set. */
	public static <K, V extends Comparable<V>> boolean putmax(Map<K, V> map, K key, V value) {
		boolean result;
		V old = map.get(key);
		if (result = (old == null || old.compareTo(value) < 0)) {
			map.put(key, value);
		}
		return result;
	}
	
	
}

